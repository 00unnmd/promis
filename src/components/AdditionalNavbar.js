import React from 'react';
import propTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Sidebar from 'react-sidebar';

//icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { faBars } from '@fortawesome/free-solid-svg-icons';

//components
import SidebarContent from './SidebarContent';

class AdditionalNavbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sidebarOpen: false,
        };

        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    onSetSidebarOpen(open) {
        this.setState({ sidebarOpen: open });
    }
    
    render() {
        return (
            <div className="navbar fixed-top">
                <div className="container">
                    <Link to='/' className={ `nav-link ${this.props.titleColor}` }>
                        <FontAwesomeIcon icon={faArrowLeft} />
                    </Link>
                    <h5 className={ `nav-link ${this.props.titleColor}` }>
                        {this.props.pagename}
                    </h5>
                    <button className={ `nav-link sidebar-btn ${this.props.titleColor}` } onClick={() => this.onSetSidebarOpen(true)}>
                        <FontAwesomeIcon icon={faBars} />
                    </button>
                </div>
                <div className="sidenav-wrap">
                    <Sidebar
                        sidebar={
                            <SidebarContent onSetSidebarOpen={this.onSetSidebarOpen} />
                        }
                        open={this.state.sidebarOpen}
                        pullRight={true}
                        onSetOpen={this.onSetSidebarOpen}
                        styles={
                            {
                                root: { position: 'unset' },
                                sidebar: { background: 'white', height: '100vh', width: '200px' },
                                content: { position: 'unset' } 
                            }
                        }
                    >
                        <div></div>
                    </Sidebar>
                </div>
            </div>
        );
    }
}

AdditionalNavbar.propTypes = {
    titleColor: propTypes.string.isRequired,
    pagename: propTypes.string.isRequired,
};

export default AdditionalNavbar;