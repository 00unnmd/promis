import React from 'react';

//icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';

//components
import MapWrap from './MapWrap';

//styles
import '../styles/Contacts.scss';

class Contacts extends React.Component {
    render() {
        return (
            <section className="contacts componentSetUp">
                <div className="contacts__wrap container componentSetUp__wrap">
                    <MapWrap />
                    <div className="contacts__info">
                        <div className="contacts__phone-number">
                            <FontAwesomeIcon icon={faPhone} className="black-color" />
                            <div className="contacts__phone-number__wrap">
                                <a href="tel:+7(347)2937-440" className="black-color">+7(347)2937-440</a>
                                <a href="tel:+7(347)2937-439" className="black-color">+7(347)2937-439</a>
                            </div>
                        </div>
                        <div className="contacts__email">
                            <FontAwesomeIcon icon={faEnvelope} className="black-color" />
                            <a href="mailto:ic_promis@mail.ru" className="black-color">ic_promis@mail.ru</a>
                        </div>
                        <div className="contacts__address">
                            <FontAwesomeIcon icon={faMapMarkerAlt} className="black-color" />
                            <span className="black-color">г.Уфа, Менделеева 134/4 оф.305</span>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Contacts;