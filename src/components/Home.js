import React from 'react';

//styles
import '../styles/Home.scss'

class Home extends React.Component {
    render () {
        return (
            <section className="home">
                <div className="home__wrap container">
                    <div className="home__title">
                        <h1>Инженерный центр "ПроМИС"</h1>
                        <p>Проектирование и монтаж инженерных систем</p>
                    </div>
                    <div className="home__info">
                        <div className="home__phone">
                            <a href="tel:+7(347)2937-440" className="white-color">+7(347)2937-440</a>
                            <a href="tel:+7(347)2937-439" className="white-color">+7(347)2937-439</a>
                        </div>
                        <div className="home__adress">
                            <p>г.Уфа, Менделеева 134/4 оф.305</p>
                        </div>
                    </div>
                </div>
                <div className="background__wrap">
                    <div className="background__item" />
                </div>
            </section>
        );
    }
}

export default Home;