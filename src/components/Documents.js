import React from 'react';

//styles
import '../styles/Documents.scss';

class Documents extends React.Component {
    render() {
        return (
            <section className="documents componentSetUp">
                <div className="documents__wrap container componentSetUp__wrap">
                    <ul className="documents__list">
                        <li>
                            <a target="_blank" href="../documents/Выписка_ИЦ ПроМИС_(0278140402)_от_31-10-2019_watermark.pdf">Выписка из реестра членов саморегулируемой организации</a> 
                        </li>
                        <li>
                            <a target="_blank" href="../documents/Лицензия минпромторг_watermark.pdf">Лицензия министерства промышленности и торговли</a> 
                        </li>
                        <li>
                            <a target="_blank" href="../documents/Лицензия МЧС_watermark.pdf">Лицензия МЧС</a> 
                        </li>
                        <li>
                            <a target="_blank" href="../documents/Свидетельство о постановке на налоговый учет_watermark.pdf">Свидетельство о постановке на налоговый учет</a> 
                        </li>
                        <li>
                            <a target="_blank" href="../documents/фсб сов секр_watermark.pdf">Лицензия на проведение работ связанных с государственной тайной</a> 
                        </li>
                    </ul>
                </div>
            </section>
        );
    }
}

export default Documents;