import React from 'react'
import propTypes from 'prop-types';
import Sidebar from 'react-sidebar';

//icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';

//components
import SidebarContent from './SidebarContent'

class MobileMainNavbar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sidebarOpen: false,
        };

        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    }

    onSetSidebarOpen(open) {
        this.setState({ sidebarOpen: open });
    }

    render() {
        return(
            <div className="navbar fixed-top">
            <div className="container">
                <button className={ `nav-link right-side sidebar-btn ${this.props.titleColor}` } onClick={() => this.onSetSidebarOpen(true)}>
                    <FontAwesomeIcon icon={faBars} />
                </button>
            </div>
            <div className="sidenav-wrap">
                <Sidebar
                    sidebar={
                        <SidebarContent onSetSidebarOpen={this.onSetSidebarOpen} />
                    }
                    open={this.state.sidebarOpen}
                    pullRight={true}
                    onSetOpen={this.onSetSidebarOpen}
                    styles={
                        {
                            root: { position: 'unset' },
                            sidebar: { background: 'white', height: '100vh', width: '200px' },
                            content: { position: 'unset' } 
                        }
                    }
                >
                    <div></div>
                </Sidebar>
            </div>
        </div>
        );
    }
}

MobileMainNavbar.propTypes = {
    titleColor: propTypes.string,
}

export default MobileMainNavbar;