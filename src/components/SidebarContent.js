import React from 'react';
import { Link } from 'react-router-dom';

//icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

class SidebarContent extends React.Component {
    render() {
        return (
            <div className="sidebar-content">
                <Link to='/' className='nav-link black-color' onClick={() => {this.props.onSetSidebarOpen(false)}}>Главная</Link>
                <Link to='/about' className='nav-link black-color' onClick={() => {this.props.onSetSidebarOpen(false)}}>О нас</Link>
                <Link to='/projects' className='nav-link black-color' onClick={() => {this.props.onSetSidebarOpen(false)}}>Проекты</Link>
                <Link to='/partners' className='nav-link black-color' onClick={() => {this.props.onSetSidebarOpen(false)}}>Партнёры</Link>
                <Link to='/documents' className='nav-link black-color' onClick={() => {this.props.onSetSidebarOpen(false)}}>Документы</Link>
                <Link to='/contacts' className='nav-link black-color' onClick={() => {this.props.onSetSidebarOpen(false)}}>Контакты</Link>
                <button className='close sidebar-btn' onClick={() => {this.props.onSetSidebarOpen(false)}}>
                    <FontAwesomeIcon icon={faTimes} />
                </button>
            </div>
            
        );
    }
}

export default SidebarContent;