import React from 'react';
import { Switch, Route } from 'react-router-dom';

//components
import Home from './Home';
import About from './About';
import Projects from './Projects';
import Partners from './Partners';
import Documents from './Documents';
import Contacts from './Contacts';

class Main extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/about' component={About} />
                <Route path='/projects' component={Projects} />
                <Route path='/partners' component={Partners} />
                <Route path='/documents' component={Documents} />
                <Route path='/contacts' component={Contacts} />
            </Switch>
        );
    }
}

export default Main;