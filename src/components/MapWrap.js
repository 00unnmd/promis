import React from 'react';
import GMaps from 'google-map-react';

const API_KEY = 'AIzaSyD6xoVAtujnT14M1KSqF8bagE0HhHq6neU';
const MapContent = ({text}) => (    //for label
    <div className="contacts__map__label">
        {text}
    </div>
);

class MapWrap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            center: {lat: 54.713444, lng: 56.008521},
            zoom: 17,
            text: 'Инженерный центр "ПроМИС"'
        }
    }
    
    render() {
        return(
            <div className="contacts__map">
                <GMaps
                    bootstrapURLKeys={{ key: API_KEY }}
                    defaultCenter={this.state.center}
                    defaultZoom={this.state.zoom}
                >
                    <MapContent
                        lat={this.state.center.lat}
                        lng={this.state.center.lng}
                        text={this.state.text} 
                    />
                </GMaps>
            </div>
        );
    }
}

export default MapWrap;