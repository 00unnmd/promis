import React from 'react';
import { Link } from 'react-router-dom';

//components
import MobileMainNavbar from './MobileMainNavbar';

//styles
import '../styles/Navbar.scss';

const clientWidth = document.documentElement.clientWidth;

class DesktopMainNavbar extends React.Component {
    render() {
        if (clientWidth < 768) {
            return (
                <MobileMainNavbar titleColor='white-color' />
            );
        }
        else {
            return (
                <div className="navbar fixed-top">
                    <div className="container">
                        <Link to='/about' className='nav-link white-color'>О нас</Link>
                        <Link to='/projects' className='nav-link white-color'>Проекты</Link>
                        <Link to='/partners' className='nav-link white-color'>Партнёры</Link>
                        <Link to='/documents' className='nav-link white-color'>Документы</Link>
                        <Link to='/contacts' className='nav-link white-color'>Контакты</Link>
                    </div>
                </div>
            );
        }
    }
}

export default DesktopMainNavbar;