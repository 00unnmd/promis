import React from 'react';
import { Switch, Route } from 'react-router-dom';

//components
import DesktopMainNavbar from './DesktopMainNavbar';
import AdditionalNavbar from './AdditionalNavbar';

class Header extends React.Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' render={() => <DesktopMainNavbar /> } />
                <Route path='/about' render={() => <AdditionalNavbar pagename='О нас' titleColor='white-color'/> } />
                <Route path='/projects' render={() => <AdditionalNavbar pagename='Проекты' titleColor='black-color'/> } />
                <Route path='/partners' render={() => <AdditionalNavbar pagename='Партнёры'/> } />
                <Route path='/documents' render={() => <AdditionalNavbar pagename='Документы' titleColor='white-color'/> } />
                <Route path='/contacts' render={() => <AdditionalNavbar pagename='Контакты' titleColor='black-color'/> } />
            </Switch>
        );
    }
}

export default Header;