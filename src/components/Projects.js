import React from 'react';
import $ from 'jquery';

//styles
import '../styles/Projects.scss';
import '../styles/Slider-projects.scss';
import 'bootstrap/dist/js/bootstrap.min.js';

const adaptate = () => { 
    $('.carousel').carousel({// добавляет возможность листать слайды на мобильных устройствах с помощью свайпов
        touch: true
    });

    if (window.innerWidth < 560) { // на мобильных устройствах отображается по 3 проекта на слайд
        wrapItems(3);
    } 

    window.addEventListener('resize', resizeProjects); //при ресайзе количество проектов в слайде будет меняться
}

const wrapItems = (count) => { // функция собирает все проекты в массив и распределяет по числу указанному в count в каждом слайде: count=3 => в слайде будет 3 проекта
    let arr = [];
    let counter = Array.from(document.querySelectorAll(".projects__item")); // собираем все проекты в массив

    for(let i = 0; i < counter.length; i = i + count) {
        arr.push(counter.slice(i, i + count)); // собираем проекты в подмассив, кол-во проектов в подмассиве зависит от count
    }

    document.querySelector(".slider-projects__inner").innerHTML = null; // очищаем слайдер

    arr.forEach((item) => { //для каждого подмассива создаем элемент "слайд" и в него отправляем все элементы подмассива
        let carousel_item = document.createElement("div");
        carousel_item.classList.add("carousel-item");
        carousel_item.classList.add("slider-projects__item");
        let projects_grid = document.createElement("div");
        projects_grid.classList.add("projects__grid");
        let carousel_inner = document.querySelector(".slider-projects__inner");
        carousel_inner.append(carousel_item);
        carousel_item.append(projects_grid);
        item.forEach((i) => {
            projects_grid.append(i);
        })
    });

    document.querySelector(".slider-projects__item").classList.add("active"); // добавляем первому слайду класс active, чтобы слайдер заработал
    document.querySelector(".slider-projects__indicators").innerHTML = null; // очищаем блок с индикаторами

    for(let i = 0; i < arr.length; i++) { // обновляем блок с индикаторами, так как после каждого ресайза должно быть выведено соответствующее кол-во индикаторов
        let li = document.createElement("li");
        li.setAttribute("data-target", "#slider-projects");
        li.setAttribute("data-slide-to", i + 1);
        document.querySelector(".slider-projects__indicators").append(li);
    }

    document.querySelector(".slider-projects__indicators").firstChild.classList.add("active");
    arr = [];
}

const resizeProjects = () => {
    if (window.innerWidth < 560) {
        wrapItems(3);
    }
    else {
        wrapItems(6);
    }
}

class Projects extends React.Component {
    componentDidMount() {
        adaptate();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', resizeProjects);
    }

    render() {
        return (
            <section className="projects">
                <div className="projects__wrap">
                    <div id="slider-projects" className="carousel slide slider-projects" data-ride="carousel">
                        <ol className="carousel-indicators slider-projects__indicators">
                            <li data-target="#slider-projects" data-slide-to="0" className="active"></li>
                            <li data-target="#slider-projects" data-slide-to="1"></li>
                        </ol>
                        <div className="carousel-inner slider-projects__inner">
                            <div className="carousel-item slider-projects__item active">
                                <div className="projects__grid">
                                    <div className="projects__item" data-description="inn"> 
                                        <p className="projects__item-description projects__item-description--large">Гостиница Holiday Inn г. Уфа</p>
                                    </div>
                                    <div className="projects__item" data-description="enthuziast"> 
                                        <p className="projects__item-description">Квартал "Энтузиастов" г. Уфа</p>
                                    </div>
                                    <div className="projects__item" data-description="gd"> 
                                        <p className="projects__item-description">МФК "Гостиный двор"</p>
                                    </div>
                                    <div className="projects__item" data-description="kerch"> 
                                        <p className="projects__item-description">Мост через Керченский пролив</p> 
                                    </div>
                                    <div className="projects__item" data-description="airport"> 
                                        <p className="projects__item-description projects__item-description--large">Международный аэропорт "Уфа"</p> 
                                    </div>
                                    <div className="projects__item" data-description="transneft"> 
                                        <p className="projects__item-description">Транснефть Урал</p>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item slider-projects__item">
                                <div className="projects__grid">
                                    <div className="projects__item" data-description="rshbank"> 
                                        <p className="projects__item-description">Россельхозбанк</p> 
                                    </div>
                                    <div className="projects__item" data-description="kerchpark"> 
                                        <p className="projects__item-description">Комсомольски парк г. Керчь</p>
                                    </div>
                                    <div className="projects__item" data-description="lukoil"> 
                                        <p className="projects__item-description">Лукойл Уралнефтепродукт</p>
                                    </div>
                                    <div className="projects__item" data-description="davlekanovokhp"> 
                                        <p className="projects__item-description projects__item-description--large">Давлекановский КХП</p>
                                    </div>
                                    <div className="projects__item" data-description="unhbashneft"> 
                                        <p className="projects__item-description projects__item-description--large">УНХ Башнефть</p> 
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <a className="carousel-control-prev slider-projects__prev-button" href="#slider-projects" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon slider-projects__prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next slider-projects__next-button" href="#slider-projects" role="button" data-slide="next">
                            <span className="carousel-control-next-icon slider-projects__next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </section>
        );
    }
}

export default Projects;