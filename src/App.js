import React from 'react';

//components
import Header from './components/Header';
import Main from './components/Main';

//styles
import './styles/ContentLayout.scss';


const heightCalculating = () => {
    let vh = window.innerHeight;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}

export default class App extends React.Component {
    componentDidMount = () => {
        heightCalculating();
        window.addEventListener('resize', () => {
            setTimeout(() => {
                heightCalculating();
            }, 100)
        })
    }

    render() {
        return (
            <div className="contentLayout">
                <Header />
                <Main />
            </div>
        );
    }
}